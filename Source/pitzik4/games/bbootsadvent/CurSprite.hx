package pitzik4.games.bbootsadvent;

import flixel.FlxSprite;
import flixel.FlxG;
import flixel.util.FlxPoint;
import flixel.FlxCamera;

import flash.display.BitmapData;
import flash.Lib;

import openfl.Assets;

class CurSprite extends FlxSprite {
  inline public static var ImgCursor:String = "assets/gfx/cursor.png";
  //private static var _pZero:FlxPoint = new FlxPoint(0,0);
  
  public function new() {
    super();
    #if !FLX_NO_MOUSE
    immovable = true;
    loadGraphic(ImgCursor, true);
    animation.add("no", [0]);
    animation.add("yes", [1]);
    animation.play("yes");
    scrollFactor.x = scrollFactor.y = 0;
    #else
    visible = false;
    #end
  }
  
  override public function update():Void {
    super.update();
    #if !FLX_NO_MOUSE
    x = FlxG.mouse.screenX; y = FlxG.mouse.screenY;
    if(FlxG.mouse.screenX <= 0) {
      visible = false;
    } else {
      visible = true;
    }
    #end
  }
}
