package pitzik4.games.bbootsadvent.ui;

import flixel.text.FlxText;
import flixel.FlxSprite;
import flixel.ui.FlxClickArea;
import flixel.ui.FlxButton;

class UICheckbox extends FlxSprite {
  private var label:FlxText;
  private var clickArea:FlxClickArea;
  public var checked:Bool;
  private var onClick:Dynamic;
  
  public function new(x:Float, y:Float, width:Int=100, ?text:String, checked:Bool=false, ?onClick:Dynamic) {
    super(x, y);
    this.onClick = onClick;
    clickArea = new FlxClickArea(x, y, width, 20, check);
    if(text != null) {
      label = new FlxText(x+24, y+3, width-24, text);
    }
    loadGraphic("assets/gfx/checkbox.png", 20, 20);
    animation.add("uchecked", [0]);
    animation.add("ucheckedh", [1]);
    animation.add("checked", [2]);
    animation.add("checkedh", [3]);
    if(checked) {
      animation.play("checked");
    }
    this.checked = checked;
  }
  
  public function check() {
    checked = !checked;
    if(onClick != null)
      Reflect.callMethod(null, onClick, []);
  }
  override public function update() {
    label.x = x+24;
    label.y = y+3;
    clickArea.x = x;
    clickArea.y = y;
    clickArea.update();
    var anim:String;
    if(checked) {
      anim = "checked";
    } else {
      anim = "uchecked";
    }
    if(clickArea.status != FlxButton.NORMAL) {
      anim += "h";
    }
    animation.play(anim);
  }
  override public function draw() {
    super.draw();
    if(label != null) {
      label.cameras = cameras;
      label.draw();
    }
  }
}
