package pitzik4.games.bbootsadvent;

import flixel.FlxState;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxCamera;
import flixel.util.FlxPoint;

import pitzik4.games.bbootsadvent.char.Player;

class PlayState extends FlxState {
  private var player:Player;
  
  override public function create():Void {
    FlxG.cameras.bgColor = 0xFF40B0F0;
    
    player = new Player(184, 134);
    add(player);
    
    add(new CurSprite());
    onResize(FlxG.stage.stageWidth, FlxG.stage.stageHeight);
  }
  override public function onResize(width:Int, height:Int):Void {
    super.onResize(width, height);
    width = cast width/FlxG.camera.zoom;
    height = cast height/FlxG.camera.zoom;
    FlxG.camera.setSize(width, height);
    FlxG.camera.follow(player, FlxCamera.STYLE_TOPDOWN);
    var fs = FlxG.camera._flashSprite;
    fs.x = FlxG.camera.x + FlxG.camera._flashOffsetX;
    fs.y = FlxG.camera.y + FlxG.camera._flashOffsetY;
  }
  override public function update():Void {
    super.update();
  }
}
