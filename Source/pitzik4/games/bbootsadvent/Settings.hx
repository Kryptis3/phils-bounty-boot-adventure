package pitzik4.games.bbootsadvent;

#if flash
import flash.net.SharedObject;
#elseif !html5
import sys.io.File;
import sys.FileSystem;
import haxe.Json;
#end

class Settings {
  inline public static var CFG_FILE_NAME:String = "hgsettings.json";
  inline public static var CFG_SO_NAME:String = "hgsettings";
  public static var fullscreen:Bool = false;
  public static var redirectTraces:Bool = true;
  
  public static function load():Void {
    #if flash
    var so:SharedObject = SharedObject.getLocal(CFG_SO_NAME);
    if(so.data.redirectTraces != null)
      redirectTraces = cast so.data.redirectTraces;
    #elseif !html5
    if(FileSystem.exists(CFG_FILE_NAME)) {
      var settings:Dynamic = Json.parse(File.getContent(CFG_FILE_NAME));
      if(settings.fullscreen != null)
        fullscreen = cast settings.fullscreen;
      if(settings.redirectTraces != null)
        redirectTraces = cast settings.redirectTraces;
    }
    #end
  }
  public static function save():Void {
    #if flash
    var so:SharedObject = SharedObject.getLocal(CFG_SO_NAME);
    so.setProperty("redirectTraces", redirectTraces);
    #elseif !html5
    File.saveContent(CFG_FILE_NAME, Json.stringify({fullscreen:fullscreen,redirectTraces:redirectTraces}));
    #end
  }
}
